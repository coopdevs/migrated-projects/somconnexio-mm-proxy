from __future__ import absolute_import, unicode_literals

import os
import bugsnag

from celery import Celery
from .exception_handler import custom_connect_failure_handler

bugsnag.configure(
    api_key=os.getenv("BUGSNAG_SOMCONNEXIO_MM_PROXY"),
    release_stage=os.getenv("BUGSNAG_RELEASE_STAGE"),
)

# set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "masmovil_proxy.settings")

app = Celery("masmovil_proxy")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object("django.conf:settings", namespace="CELERY")

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

# Connect to custom exceptions handler
custom_connect_failure_handler()


@app.task(bind=True)
def debug_task(self):
    print("Request: {0!r}".format(self.request))
