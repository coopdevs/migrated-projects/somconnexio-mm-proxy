from celery.signals import task_failure

from otrs.services.update_ticket_with_error import UpdateTicketWithError
from api.exceptions import ExceptionToSendToOTRS


def failure_handler(sender, task_id, exception, args, kwargs, traceback, einfo, **kw):
    """
    Communicate to OTRS all ExceptionToSendToOTRS exceptions
    """
    if isinstance(exception, ExceptionToSendToOTRS):
        UpdateTicketWithError(
            exception.ticket_id, {"missatge": exception.message},
            exception.action, exception.object_type
        ).run()


def custom_connect_failure_handler():
    """
    Connect the our failure_handler to the Celery task_failure signal
    """
    task_failure.connect(failure_handler, weak=False)
