from otrs_somconnexio.services.update_ticket_with_provider_info import (
    UpdateTicketWithProviderInfo,
)

from otrs.models.error_article import ErrorArticle


class UpdateTicketWithError(UpdateTicketWithProviderInfo):
    """
    Update the ticket process with an article to inform about an error.

    Params:
    ticket_id (str) -> id from the involved OTRS ticket to which an article will be send
    error (dict) -> dictionary with the error description
    action (str) -> action that failed (ex: get / create )
    object_type (str) -> class which had the problem: account, order-item or asset
    """

    def __init__(self, ticket_id, error, action, object_type):
        self.ticket_id = ticket_id
        self.article = ErrorArticle(error, action, object_type)
        self.df_dct = {
            "dataIntroPlataforma": "",
            "dataActivacioLiniaMobil": "",
            "permetActivacio": "",
        }

        if (
            action == "resolució" and object_type == "OrderItem"
        ) or object_type == "Asset":
            self.df_dct.update(
                {
                    "errorActivacio": 1,
                    "enviarCheckOrderItem": 0,
                    "MMOrderItemId": "",
                }
            )
        # TODO: Big refactor of exception rasing to notify OTRS!
        elif (action == "processement" and object_type == "OrderItem"):
            self.df_dct.update(
                {
                    "errorActivacio": 1,
                    "enviarCheckOrderItem": 0,
                }
            )
        elif action == "addició" and object_type == "abonament one-shot addicional":
            self.df_dct = {
                "dadesKO": 1,
            }
        elif action == "canvi de tarifa":
            self.df_dct = {
                "canviKO": 1,
            }
        else:
            self.df_dct.update(
                {
                    "errorPeticio": 1,
                    "enviarCreateOrderItem": 0,
                }
            )
