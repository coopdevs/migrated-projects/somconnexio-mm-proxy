# coding: utf-8
from otrs_somconnexio.otrs_models.abstract_article import AbstractArticle


class AssetArticle(AbstractArticle):
    """
    Creates an article informing that a phone contract has been activated
    in the MM platform.

    Params:
    aseset (object) -> MM asset instance
    """

    def __init__(self, asset):
        self.subject = "Línia de mòbil donada d'alta a MasMóvil"
        self.body = self._body_from_dict(asset)

    def _body_from_dict(self, dct):
        body = ""

        for field in sorted(dct):
            value = dct[field]
            if value:
                body = u"{}{}: {}\n".format(body, field, value)

        return body
