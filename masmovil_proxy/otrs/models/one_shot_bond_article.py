# coding: utf-8
from otrs_somconnexio.otrs_models.abstract_article import AbstractArticle


class OneShotBondArticle(AbstractArticle):
    """
    Creates an article informing that the one shot additional bond request
    has been succesfully completed

    Params:
    body (str) -> custom message
    """

    def __init__(self, body):
        self.subject = "Abonament addicional one-shot correctament realitzat"
        self.body = body
