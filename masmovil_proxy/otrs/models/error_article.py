# coding: utf-8
from otrs_somconnexio.otrs_models.abstract_article import AbstractArticle


class ErrorArticle(AbstractArticle):
    """
    Creates an article with the error data that explains what whent wrong in the
    request processing.

    Params:
    error (dict) -> dictionary with the error description
    action (str) -> action that failed (ex: get / create )
    object_type (str) -> class which had the problem: account, order-item or asset
    """

    def __init__(self, error, action, object_type):
        self.subject = "Error desde Más Móvil en el/la {} d'un/a {}".format(
            action, object_type
        )
        self.body = self._body_from_dict(error)

    def _body_from_dict(self, dct):
        body = ""

        for field in sorted(dct):
            value = dct[field]
            if value:
                body = u"{}{}: {}\n".format(body, field, value)

        return body
