from api.models import Province
from api.models import RoadType
from api.exceptions import PostalCodeFormatException
from api.services.vat import VAT
from api.somconnexio_contact import SomConnexioContact


class AccountMapping:
    def __init__(self, ticket_data, customer_data):
        """
        Build an object with the attributes required to create an Account in the MM API.
        Fill these attributes with OTRS data
        """

        vat = VAT(customer_data.get("UserIdCode"))

        if customer_data.get("UserPartyType") == "person":
            self.name = customer_data.get("UserFirstname")
            self.nationality = customer_data.get("UserCountryCode")
            # MM code corresponding to "NIE" documentType -> 3, "NIF" -> 2
            self.documentType = "3" if vat.contains_NIE() else "2"
        else:  # organization
            self.name = customer_data.get("UserLastname")
            self.corporateName = customer_data.get("UserLastname")
            self.documentType = "1"  # MM code corresponding to "CIF" documentType

        self.email = SomConnexioContact.email
        self.phone = SomConnexioContact.phone
        self.surname = customer_data.get("UserLastname")
        self.documentNumber = vat.remove_VAT_format()
        self.province = Province.search_by_OTRS_code(
            customer_data.get("UserSubdivisionCode")
        ).mm_code
        self.region = Province.search_by_OTRS_code(
            customer_data.get("UserSubdivisionCode")
        ).region.mm_code
        self.postalCode = self.check_postal_code(customer_data.get("UserZip"))
        self.town = customer_data.get("UserCity")
        self.roadName = ticket_data.get("road_name") or None
        self.roadNumber = ticket_data.get("road_number") or None
        self.roadType = RoadType.search_by_OTRS_code(
            ticket_data.get("road_type")
        ).mm_code
        self.stair = ticket_data.get("stair") or None
        self.flat = ticket_data.get("flat") or None
        self.door = ticket_data.get("door") or None
        self.buildingPortal = ticket_data.get("building_portal") or None

    def check_postal_code(self, pc):
        """
        Postal codes from Tryton are entered manually from the web (no validations are done)
        Many incorrect 4 letter postal code entries from around Barcelona are incorrect because
        users forget to add the first 0. In here we add it as a guess to try to correct it.
        MM has its own postal code validation together with the rest of the address.

        TODO -> A real postal code validation must be done at the ERP level. See:
        https://www.correos.es/dinamic/Servicios-Web/WebServiceCodigoPostal/CodigoPostal.asmx?op=ConsultarCodigos
        """
        pc = pc.strip()

        if len(pc) == 4 and pc.startswith("8"):
            pc = "0" + pc
        if pc.isdigit() and len(pc) == 5:
            return pc
        else:
            raise PostalCodeFormatException(pc)
