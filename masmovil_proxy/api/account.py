from celery.utils.log import get_task_logger
from bugsnag.celery import connect_failure_handler
from pymasmovil.models.account import Account as MMAccount
from pymasmovil.errors.http_error import HTTPError
from pymasmovil.errors.exceptions import AccountRequiredParamsError

from otrs_somconnexio.otrs_models.customer_user import CustomerUser
from otrs.services.update_ticket_with_error import UpdateTicketWithError

from api.exceptions import PostalCodeFormatException, ProvinceNotFoundException
from api.account_mapping import AccountMapping
from api.OTRS_catalan_messages import INCOMPLETE_ACCOUNT_MSG
from api.services.vat import VAT

logger = get_task_logger(__name__)
connect_failure_handler()


class Account:
    def __init__(self, session, ticket_data, customer_id, ticket_id):
        self.session = session
        self.ticket_data = ticket_data
        self.ticket_id = ticket_id
        self.customer_user_data = CustomerUser(customer_id).get_data()

    def get_or_create(self):
        """
        Checks if an account in the MM platform already exists for a user
        (using its NIF as ID key), and returns it if found.
        Otherwise, creates an account into the MM platform
        with the data from the request, and returns it.
        """

        user_VAT = self.customer_user_data.get("UserIdCode")
        user_NIF = VAT(user_VAT).remove_VAT_format()

        existing_account = self._get_MM_account(user_NIF)

        return existing_account or self._create_MM_account()

    def _get_MM_account(self, NIF):
        """
        Send request to get the existing account from MM
        """
        try:
            account = MMAccount.get_by_NIF(self.session, NIF)
        except HTTPError as error:
            if error.message == "No existen Clientes para el Cableoperador":
                return None
            UpdateTicketWithError(
                self.ticket_id, error.__dict__, "obtenció", "Account"
            ).run()
            raise error

        logger.info(
            "Existing account found with id: {}, documentNumber: {}".format(
                account.id, account.documentNumber
            )
        )

        return account

    def _create_MM_account(self):
        """
        Send request to create an account to MM with an AccountMapping object
        """
        try:
            mapped_account = AccountMapping(self.ticket_data, self.customer_user_data)
        except (PostalCodeFormatException, ProvinceNotFoundException) as exception:
            UpdateTicketWithError(
                self.ticket_id, {"missatge": exception.message}, "creació", "Account"
            ).run()
            raise exception

        try:
            account = MMAccount.create(self.session, **mapped_account.__dict__)
        except AccountRequiredParamsError as exception:
            UpdateTicketWithError(
                self.ticket_id,
                {"missatge": INCOMPLETE_ACCOUNT_MSG.format(exception.message)},
                "creació",
                "Account",
            ).run()
            raise exception
        except HTTPError as error:
            UpdateTicketWithError(
                self.ticket_id, error.__dict__, "creació", "Account"
            ).run()
            raise error

        logger.info(
            "Account created with id: {}, documentNumber: {}".format(
                account.id, account.documentNumber
            )
        )

        return account
