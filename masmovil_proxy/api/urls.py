from django.urls import path

from api.views.create_order_item_view import CreateOrderItemView
from api.views.check_order_item_view import CheckOrderItemView
from api.views.change_tariff_view import ChangeTariffView
from api.views.add_one_shot_bonds_view import AddOneShotBondsView

urlpatterns = [
    path(
        "jobs/create-order-item/",
        CreateOrderItemView.as_view(),
        name="create_order_item",
    ),
    path(
        "jobs/check-order-item/", CheckOrderItemView.as_view(), name="check_order_item"
    ),
    path("jobs/change-tariff/", ChangeTariffView.as_view(), name="change_tariff"),
    path(
        "jobs/add-one-shot-bonds/",
        AddOneShotBondsView.as_view(),
        name="add_one_shot_bonds",
    ),
]
