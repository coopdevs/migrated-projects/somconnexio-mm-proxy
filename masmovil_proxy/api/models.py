from django.db import models
from django.core.exceptions import ObjectDoesNotExist

from api.exceptions import (
    ProvinceNotFoundException,
    TariffNotFoundException,
    InternationalTariffNotFoundException,
    OneShotAdditionalBondNotFoundException,
)

# TODO: Separate this file in one file per class inside the models folder


class Region(models.Model):
    id = models.CharField(max_length=2, primary_key=True)
    mm_code = models.CharField(max_length=2)
    description = models.CharField(max_length=255)


class Province(models.Model):
    id = models.CharField(max_length=2, primary_key=True)
    region = models.ForeignKey(Region, on_delete=models.PROTECT)
    mm_code = models.CharField(max_length=2, unique=True)
    OTRS_code = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255)

    @classmethod
    def search_by_OTRS_code(cls, code):
        try:
            return Province.objects.get(OTRS_code=code)
        except ObjectDoesNotExist:
            raise ProvinceNotFoundException(code)


class DonorOperator(models.Model):
    id = models.CharField(max_length=3, primary_key=True)
    mm_code = models.CharField(max_length=3)
    OTRS_code = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255)

    @classmethod
    def search_by_OTRS_code(cls, code):
        try:
            return DonorOperator.objects.get(OTRS_code=code)
        except ObjectDoesNotExist:
            return DonorOperator.objects.get(OTRS_code="other")


class NewLineRequest(models.Model):
    def get_transaction_id(self):
        return "{:018d}".format(self.id)


class Tariffs(models.Model):
    product_code = models.CharField(max_length=40)
    mm_tariff = models.CharField(max_length=18)
    additional_bond = models.CharField(max_length=18, blank=True)
    additional_bond2 = models.CharField(max_length=18, blank=True)
    promotion = models.CharField(max_length=18, blank=True)

    @classmethod
    def search_by_product_code(cls, code):
        try:
            return Tariffs.objects.get(product_code=code)
        except ObjectDoesNotExist:
            raise TariffNotFoundException(code)

    @classmethod
    def get_all_recurrent_additional_bonds(cls):
        current_tariffs = Tariffs.objects.all()
        # TRV bonds are recurrent
        current_bonds = OneShotAdditionalBonds.objects.all()
        bonds_set = set(
            [tariff.additional_bond for tariff in current_tariffs if tariff.additional_bond]
            + [tariff.additional_bond2 for tariff in current_tariffs if tariff.additional_bond2]
            + [bond.trv_bond for bond in current_bonds if bond.trv_bond]
        )
        return [*bonds_set, ]


class TRVProducts(models.Model):
    product_code = models.CharField(max_length=40, unique=True)

    @classmethod
    def get_product_codes(cls):
        return [product.product_code for product in TRVProducts.objects.all()]


class OneShotAdditionalBonds(models.Model):
    product_code = models.CharField(max_length=40, unique=True)
    additional_bond = models.CharField(max_length=18, unique=True)
    trv_bond = models.CharField(max_length=18, null=True)

    @classmethod
    def search_by_product_code(cls, code):
        try:
            return OneShotAdditionalBonds.objects.get(product_code=code)
        except ObjectDoesNotExist:
            raise OneShotAdditionalBondNotFoundException(code)


class InternationalTariffs(models.Model):
    minutes = models.CharField(max_length=10)
    mm_tariff = models.CharField(max_length=18, unique=True)

    @classmethod
    def search_MM_tariff(cls, minutes):
        try:
            return InternationalTariffs.objects.get(minutes=minutes)
        except ObjectDoesNotExist:
            raise InternationalTariffNotFoundException(minutes)


class RoadType(models.Model):
    mm_code = models.CharField(max_length=255)
    OTRS_code = models.CharField(max_length=255, unique=True)

    @classmethod
    def search_by_OTRS_code(cls, code):
        try:
            return cls.objects.get(OTRS_code=code)
        except ObjectDoesNotExist:
            return cls.objects.get(OTRS_code="carrer")
