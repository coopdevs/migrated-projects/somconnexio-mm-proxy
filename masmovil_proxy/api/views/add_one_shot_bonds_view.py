import logging

from rest_framework.views import APIView
from rest_framework.response import Response
from api.tasks import add_one_shot_bonds

logger = logging.getLogger(__name__)


class AddOneShotBondsView(APIView):
    def patch(self, request):

        request_data = request.data

        logger.info(
            "One shot additional bonds request recieved. Request body: {}".format(
                request_data
            )
        )

        add_one_shot_bonds.apply_async(
            kwargs={"request_body": request_data}, queue="mm-proxy"
        )

        return Response()
