from mock import patch

from django.test import TestCase
from django.urls import reverse


class ChangeTariffViewTestCase(TestCase):
    @patch("api.views.change_tariff_view.change_tariff")
    def test_patch(self, mock_change_tariff_task):
        """
        Check that a patch request is done to the ChangeTariffView endpoint
        with the expected body.
        Also, prove that a task is enqueued with celery
        """
        self.new_request = {"test": "data"}

        url = reverse("change_tariff")

        response = self.client.patch(
            url, data=self.new_request, content_type="application/json"
        )

        mock_change_tariff_task.apply_async.assert_called_with(
            kwargs={"request_body": self.new_request}, queue="mm-proxy"
        )
        self.assertEqual(response.status_code, 200)
