from mock import patch

from django.test import TestCase
from django.urls import reverse


class CreateOrderItemViewTestCase(TestCase):
    @patch("api.views.create_order_item_view.create_order_item")
    def test_post(self, mock_create_order_item_task):
        """
        Check that a post request is done to the CreateOrderItemView endpoint
        with the expected body.
        Also, prove that a task is enqueued with celery
        """
        self.new_request = {"test": "data"}

        url = reverse("create_order_item")

        response = self.client.post(
            url, data=self.new_request, content_type="application/json"
        )

        mock_create_order_item_task.apply_async.assert_called_with(
            kwargs={"request_body": self.new_request}, queue="mm-proxy"
        )
        self.assertEqual(response.status_code, 200)
