# coding: utf-8
from datetime import date

from django.test import TestCase
from api.services.date import Date


class DateTestCase(TestCase):

    def test_get_first_day_of_next_month(self):
        today = date.today()
        if today.month == 12:
            first_day_next_month = date(today.year + 1, 1, 1)
        else:
            first_day_next_month = date(today.year, today.month + 1, 1)

        change_tariff_date = Date.get_first_day_of_next_month()

        self.assertEqual(change_tariff_date, first_day_next_month.strftime("%Y-%m-%d"))
