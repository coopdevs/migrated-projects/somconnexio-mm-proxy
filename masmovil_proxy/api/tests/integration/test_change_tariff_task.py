import os
from mock import patch, Mock
from django.test import TestCase
from pyotrs.lib import DynamicField

from pymasmovil.models.session import Session
from pymasmovil.models.asset import Asset as MMAsset
from otrs_somconnexio.client import OTRSClient

from api.OTRS_catalan_messages import TARIFF_CHANGED_MSG
from api.models import NewLineRequest
from api.tasks import change_tariff


class ChangeTariffTaskIntegrationTestCase(TestCase):
    fixtures = ["api/tests/fixtures/test_db"]

    @patch.object(Session, "create")
    @patch.object(MMAsset, "get_by_phone")
    @patch.object(MMAsset, "update_product")
    @patch.object(OTRSClient, "update_ticket")
    @patch("otrs_somconnexio.otrs_models.abstract_article.Article")
    @patch("otrs_somconnexio.services.update_ticket_with_provider_info.DynamicField")
    @patch("otrs_somconnexio.client.Client")
    @patch.dict(
        os.environ,
        {
            "OTRS_USER": "user",
            "OTRS_PASSW": "passw",
            "OTRS_URL": "https://otrs-url.coop/",
        },
    )
    def test_change_tariff(
        self,
        MockOTRSClient,
        MockDF,
        MockPyOTRSArticle,
        mock_update_ticket,
        mock_asset_update_product,
        mock_asset_get_by_phone,
        mock_session_create,
    ):
        """
        Check that from an API call we send a mobile change tariff petition to the MM API
        and the result is sent back to OTRS
        """
        phone = "666666666"
        asset_id = "000000123"
        change_date = "2022-01-18 12:00:00"
        formatted_change_date = "2022-01-18"
        request_body = {
            "ticket_id": 111,
            "phone_number": phone,
            "product_code": "SE_SC_REC_MOBILE_T_0_800",
            "change_date": change_date,
        }
        # MM product corresponding to product_code 'SE_SC_REC_MOBILE_T_0_700'
        MM_product_id = "T00000000000000001"
        asset = MMAsset(id=asset_id, phone=phone,
                        productId=MM_product_id)

        pyOTRS_article = object()
        article_params = {
            "Subject": "Canvi de tarifa correctament realitzat",
            "Body": TARIFF_CHANGED_MSG.format(
                asset_id, request_body["product_code"], formatted_change_date
            ),
            "ContentType": "text/plain; charset=utf8",
            "IsVisibleForCustomer": "1"
        }
        df_canviOK = DynamicField(name="canviOK", value=0)

        def mock_df_side_effect(name, value):
            if name == "canviOK" and value == 1:
                return df_canviOK

        mock_session = Mock(set_spec=["id"])
        mock_session.id = "0001"
        mock_session_create.return_value = mock_session
        mock_asset_get_by_phone.return_value = asset
        MockDF.side_effect = mock_df_side_effect
        MockPyOTRSArticle.return_value = pyOTRS_article

        # Task call
        change_tariff(request_body)

        # Test asserts
        mock_asset_get_by_phone.assert_called_once_with(mock_session, phone)
        mock_asset_update_product.assert_called_once_with(
            mock_session,
            asset_id=asset_id,
            transaction_id=NewLineRequest.objects.all().last().get_transaction_id(),
            product_id="T00000000000000002",
            execute_date=formatted_change_date,
            additional_bonds=[],
            promotions=[],
        )
        MockPyOTRSArticle.assert_called_once_with(article_params)
        mock_update_ticket.assert_called_once_with(
            111, pyOTRS_article, dynamic_fields=[df_canviOK]
        )
