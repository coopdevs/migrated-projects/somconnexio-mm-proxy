import os
from mock import patch, Mock
from django.test import TestCase
from pyotrs.lib import DynamicField

from pymasmovil.models.session import Session
from pymasmovil.models.account import Account as MMAccount
from pymasmovil.errors.http_error import HTTPError
from pymasmovil.models.order_item import OrderItem as MMOrderItem
from otrs_somconnexio.client import OTRSClient
from api.tasks import create_order_item
from api.tests.fake_mm_error import FakeMMError
from api.services.vat import VAT
from api.models import NewLineRequest
from api.somconnexio_contact import SomConnexioContact


class CreateOrderItemTaskIntegrationTestCase(TestCase):
    fixtures = ["api/tests/fixtures/test_db"]

    def fill_mock(self, mock, dct):
        """
        Fill mock objects with values from a dict if the keys match the mock attributes
        """
        for key, value in dct.items():
            if hasattr(mock, key):
                setattr(mock, key, value)

    @patch.object(Session, "create")
    @patch.object(MMAccount, "get_by_NIF")
    @patch.object(MMAccount, "create")
    @patch.object(MMOrderItem, "create")
    @patch("api.account.CustomerUser", return_value=Mock(spec_set=["get_data"]))
    @patch.object(OTRSClient, "update_ticket")
    @patch("otrs_somconnexio.otrs_models.abstract_article.Article")
    @patch("otrs_somconnexio.services.update_ticket_with_provider_info.DynamicField")
    @patch("otrs_somconnexio.client.Client")
    @patch.dict(
        os.environ,
        {
            "OTRS_USER": "user",
            "OTRS_PASSW": "passw",
            "OTRS_URL": "https://otrs-url.coop/",
        },
    )
    @patch("time.sleep")
    def test_create_order_item_task(
        self, _,
        MockOTRSClient,
        MockDF,
        MockPyOTRSArticle,
        mock_update_ticket,
        MockCustomerUser,
        mock_order_item_create,
        mock_account_create,
        mock_account_get,
        mock_session_create,
    ):
        """
        Check that after the request an order-item is created with an account, and their
        id's are sent to OTRS
        """

        # Empty keys from the XSLT mapped body from OTRS calls arrive as `{}`
        request_body = {
            "ticket_id": 8261,
            "customer_user": "11022",
            "ticket_account_data": {
                "road_name": "aaaa",
                "road_type": "Carrer",
                "door": {},
                "flat": {},
                "building_portal": {},
                "road_number": "2",
                "stair": {},
            },
            "ticket_order_item_data": {
                "docid": "",
                "ICC_SC": "3424344",
                "mobile_service_type": "portabilitat",
                "international_minutes": {},
                "phone_number": "699699699",
                "donor_operator": "Movistar",
                "donor_ICC": "82828382",
                "product_code": "SE_SC_REC_MOBILE_T_0_800",
            },
        }

        customer_user_data = {
            "UserPartyType": "person",
            "UserFirstname": "Guita",
            "UserLastname": "Guitona",
            "UserIdCode": "ES11111111A",
            "UserZip": "17820",
            "UserSubdivisionCode": "ES-GI",
            "UserCity": "Banyoles",
        }
        expected_account = {
            "name": "Guita",
            "surname": "Guitona",
            "documentType": "2",
            "nationality": None,
            "documentNumber": "11111111A",
            "email": SomConnexioContact.email,
            "postalCode": "17820",
            "phone": SomConnexioContact.phone,
            "province": "17",
            "region": "9",
            "roadName": "aaaa",
            "roadNumber": "2",
            "roadType": "Carrer",
            "town": "Banyoles",
            "stair": None,
            "flat": None,
            "door": None,
            "buildingPortal": None,
        }
        expected_phone_number = "699699699"
        expected_order_item = {
            "transactionId": None,
            "lineInfo": [
                {
                    "idLine": None,
                    "docid": "11111111A",
                    "phoneNumber": expected_phone_number,
                    "iccid": "3424344",
                    "iccid_donante": "82828382",
                    "productInfo": {
                        "id": "T00000000000000002",
                        "additionalBonds": [],
                    },
                    "donorOperator": "1",
                    "documentType": "NIF",
                    "name": "Guita",
                    "surname": "Guitona",
                }
            ],
        }
        expected_nif = VAT(customer_user_data.get("UserIdCode")).remove_VAT_format()

        not_found_error = FakeMMError(
            404, "No existen Clientes para el Cableoperador", ""
        )
        mock_session = Mock(set_spec=["id"])
        mock_session.id = "0001"
        mock_account = Mock(
            spec=[
                "id",
                "name",
                "surname",
                "documentNumber",
                "corporateName",
                "get_order_item_by_ICC",
            ]
        )
        self.fill_mock(
            mock_account,
            {
                "id": "000000001",
                "name": "Guita",
                "surname": "Guitona",
                "documentNumber": "11111111A",
                "corporateName": "",
            },
        )
        mock_order_item = Mock(spec=["id", "phone"])
        mock_order_item.id = "000000002"
        mock_order_item.phone = expected_phone_number
        pyOTRS_article = object()
        article_params = {
            "Subject": "Petició d'alta a MM entrada correctament",
            "Body": u"OrderItem creat a MM amb id:\t000000002\nCorresponent a l'account "
            + "amb id:\t000000001",
            "ContentType": "text/plain; charset=utf8",
            "IsVisibleForCustomer": "1"
        }

        df_introPlataforma = DynamicField(name="introPlataforma", value=1)
        df_order_item_id = DynamicField(name="MMOrderItemId", value=mock_order_item.id)
        df_checkbox = DynamicField(name="enviarCreateOrderItem", value=0)
        df_phone_number = DynamicField(name="liniaMobil", value=expected_phone_number)

        def mock_df_side_effect(name, value):
            if name == "introPlataforma" and value == 1:
                return df_introPlataforma
            elif name == "MMOrderItemId" and value == mock_order_item.id:
                return df_order_item_id
            elif name == "enviarCreateOrderItem" and value == 0:
                return df_checkbox
            elif name == "liniaMobil" and value == expected_phone_number:
                return df_phone_number

        mock_session_create.return_value = mock_session
        MockCustomerUser.return_value.get_data.return_value = customer_user_data
        mock_account_get.side_effect = HTTPError(not_found_error)
        mock_account_create.return_value = mock_account
        mock_account.get_order_item_by_ICC.return_value = mock_order_item
        MockDF.side_effect = mock_df_side_effect
        MockPyOTRSArticle.return_value = pyOTRS_article

        # Task call
        create_order_item(request_body)

        # Test asserts

        # update the expected_order_item info with transaction_id info
        created_NLR = NewLineRequest.objects.all().last()
        expected_order_item["transactionId"] = created_NLR.get_transaction_id()
        expected_order_item["lineInfo"][0]["idLine"] = created_NLR.id

        mock_session_create.assert_called_once_with()
        mock_account_get.assert_called_once_with(mock_session, expected_nif)
        mock_account_create.assert_called_once_with(mock_session, **expected_account)
        mock_order_item_create.assert_called_once_with(
            mock_session, mock_account, **expected_order_item
        )

        MockPyOTRSArticle.assert_called_once_with(article_params)
        mock_update_ticket.assert_called_once_with(
            8261,
            pyOTRS_article,
            dynamic_fields=[df_introPlataforma, df_checkbox,
                            df_order_item_id, df_phone_number],
        )
