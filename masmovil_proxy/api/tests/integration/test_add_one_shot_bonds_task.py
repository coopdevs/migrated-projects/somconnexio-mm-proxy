import os
from mock import patch, Mock
from django.test import TestCase
from pyotrs.lib import DynamicField
from datetime import datetime

from pymasmovil.models.session import Session
from pymasmovil.models.asset import Asset as MMAsset
from otrs_somconnexio.client import OTRSClient

from api.models import NewLineRequest
from api.tasks import add_one_shot_bonds
from api.OTRS_catalan_messages import ONE_SHOT_BOND_ADDED_MSG


class AddOneShotBondsTaskIntegrationTestCase(TestCase):
    fixtures = ["api/tests/fixtures/test_db"]

    @patch.object(Session, "create")
    @patch.object(MMAsset, "get_by_phone")
    @patch.object(MMAsset, "update_product")
    @patch.object(OTRSClient, "update_ticket")
    @patch("otrs_somconnexio.otrs_models.abstract_article.Article")
    @patch("otrs_somconnexio.services.update_ticket_with_provider_info.DynamicField")
    @patch("otrs_somconnexio.client.Client")
    @patch.dict(
        os.environ,
        {
            "OTRS_USER": "user",
            "OTRS_PASSW": "passw",
            "OTRS_URL": "https://otrs-url.coop/",
        },
    )
    def test_add_one_shot_bonds(
        self,
        MockOTRSClient,
        MockDF,
        MockPyOTRSArticle,
        mock_update_ticket,
        mock_asset_update_product,
        mock_asset_get_by_phone,
        mock_session_create,
    ):
        """
        Check that from an API call we send a mobile one shot bond petition to the MM API
        and the result is sended back to OTRS
        """
        phone = "666666666"
        asset_id = "000000123"
        request_body = {
            "ticket_id": 111,
            "phone_number": phone,
            "product_code": "CH_SC_OSO_500MB_ADDICIONAL",
        }
        additional_bond = {"id": "01t4I0000079CM9QAM", "qty": 1}
        pyOTRS_article = object()
        article_params = {
            "Subject": "Abonament addicional one-shot correctament realitzat",
            "Body": ONE_SHOT_BOND_ADDED_MSG.format(
                asset_id,
                request_body["product_code"],
                datetime.today().strftime("%d-%m-%Y %H:%M:%S"),
            ),
            "ContentType": "text/plain; charset=utf8",
            "IsVisibleForCustomer": "1"
        }
        df_dadesOK = DynamicField(name="dadesOK", value=1)
        df_tarifaTRV = DynamicField(name="RecuperarTarifaTRV", value=0)

        def mock_df_side_effect(name, value):
            if name == "dadesOK" and value == 1:
                return df_dadesOK
            elif name == "RecuperarTarifaTRV" and value == 0:
                return df_tarifaTRV

        mock_session = Mock(set_spec=["id"])
        mock_session.id = "0001"

        asset = MMAsset(id=asset_id, phone=phone, productId="T00000000000000002")

        mock_session_create.return_value = mock_session
        mock_asset_get_by_phone.return_value = asset
        MockDF.side_effect = mock_df_side_effect
        MockPyOTRSArticle.return_value = pyOTRS_article

        # Task call
        add_one_shot_bonds(request_body)

        # Test asserts
        mock_asset_get_by_phone.assert_called_once_with(mock_session, phone)
        mock_asset_update_product.assert_called_once_with(
            mock_session,
            asset_id=asset_id,
            transaction_id=NewLineRequest.objects.all().last().get_transaction_id(),
            additional_bonds=[additional_bond],
        )
        MockPyOTRSArticle.assert_called_once_with(article_params)
        mock_update_ticket.assert_called_once_with(
            111, pyOTRS_article, dynamic_fields=[df_dadesOK, df_tarifaTRV]
        )
