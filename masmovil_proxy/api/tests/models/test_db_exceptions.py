from django.test import TestCase

from api.models import Province, DonorOperator
from api.exceptions import ProvinceNotFoundException


class TestDbExceptionsTestCase(TestCase):
    fixtures = ["api/tests/fixtures/test_db"]

    def test_invalid_Province_mapping(self):
        self.assertRaises(
            ProvinceNotFoundException, Province.search_by_OTRS_code, "GER"
        )

    def test_invalid_DonorOperator_mapping(self):
        self.assertEqual("-", DonorOperator.search_by_OTRS_code("SC").mm_code)
        self.assertEqual("-", DonorOperator.search_by_OTRS_code(None).mm_code)
