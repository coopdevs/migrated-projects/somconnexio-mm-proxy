from django.test import TestCase

from api.models import TRVProducts


class TRVProductsTestCase(TestCase):
    fixtures = ["api/tests/fixtures/test_db"]

    def test_get_product_codes(self):
        self.assertEqual(TRVProducts.get_product_codes(), ['T00000000000000001'])
