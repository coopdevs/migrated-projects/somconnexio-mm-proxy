from mock import patch, Mock
from django.test import TestCase

from pymasmovil.models.account import Account as MMAccount
from pymasmovil.errors.http_error import HTTPError
from pymasmovil.errors.exceptions import AccountRequiredParamsError

from api.account import Account
from api.exceptions import PostalCodeFormatException, ProvinceNotFoundException
from api.tests.fake_mm_error import FakeMMError
from api.OTRS_catalan_messages import INVALID_PC_MSG, PROVINCE_NOT_FOUND_MSG
from api.services.vat import VAT


class FakeAccount:
    def __init__(self, documentNumber):
        self.documentNumber = documentNumber
        self.id = 1112


class AccountTestCase(TestCase):
    fixtures = ["api/tests/fixtures/test_db"]

    def setUp(self):
        self.ticket_id = 777
        self.document_number = "ES123456789A"
        self.customer_id = "15053"
        self.ticket_data = object()
        self.customer_user_data = {"UserIdCode": self.document_number}
        self.fake_account = FakeAccount(self.document_number)
        self.session = object()
        self.mock_otrs_customer_user = Mock(spec_set=["get_data"])
        self.mock_otrs_customer_user.get_data.return_value = self.customer_user_data

        # Errors
        self.sample_error = FakeMMError(
            400, "Debe informar el nombre del cliente", "first_name,last_name"
        )
        self.not_found_error = FakeMMError(
            404, "No existen Clientes para el Cableoperador", ""
        )
        self.expected_error = {
            "status_code": 400,
            "fields": "first_name,last_name",
            "message": "Debe informar el nombre del cliente",
        }

    def _otrs_customer_user_side_effect(self, customer_id):
        if customer_id == self.customer_id:
            return self.mock_otrs_customer_user

    @patch.object(MMAccount, "get_by_NIF")
    @patch("api.account.CustomerUser")
    def test_get_mm_account_ok(self, MockCustomerUser, mock_account_get):
        """
        Check that if the account exists it is retrieved from MM
        """
        expected_NIF = VAT(self.document_number).remove_VAT_format()
        MockCustomerUser.side_effect = self._otrs_customer_user_side_effect
        mock_account_get.return_value = self.fake_account

        account = Account(
            self.session, self.ticket_data, self.customer_id, self.ticket_id
        ).get_or_create()

        mock_account_get.assert_called_once_with(self.session, expected_NIF)
        self.assertEquals(account, self.fake_account)

    @patch.object(MMAccount, "get_by_NIF")
    @patch("api.account.CustomerUser")
    @patch("api.account.UpdateTicketWithError")
    def test_get_mm_account_ko(
        self, MockUpdateTicketWithArticle, MockCustomerUser, mock_account_get
    ):
        """
        Check that if an account exists but the MM request fails, we send the
        MM error to OTRS.
        """
        MockCustomerUser.side_effect = self._otrs_customer_user_side_effect
        mock_account_get.side_effect = HTTPError(self.sample_error)

        account = Account(
            self.session, self.ticket_data, self.customer_id, self.ticket_id
        )

        self.assertRaises(HTTPError, Account.get_or_create, account)

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id, self.expected_error, "obtenció", "Account"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAccount, "get_by_NIF")
    @patch.object(MMAccount, "create")
    @patch("api.account.AccountMapping")
    @patch("api.account.CustomerUser")
    def test_create_mm_account_ok(
        self,
        MockCustomerUser,
        MockAccountMapping,
        mock_account_create,
        mock_account_get,
    ):
        """
        Check that if the account doesn't exist we create it in MM
        """
        MockCustomerUser.side_effect = self._otrs_customer_user_side_effect
        mock_account_get.side_effect = HTTPError(self.not_found_error)
        MockAccountMapping.return_value = self.fake_account
        mock_account_create.return_value = self.fake_account

        account = Account(
            self.session, self.ticket_data, self.customer_id, self.ticket_id
        ).get_or_create()

        MockAccountMapping.assert_called_once_with(
            self.ticket_data, self.customer_user_data
        )
        mock_account_create.assert_called_once_with(
            self.session, **self.fake_account.__dict__
        )
        self.assertEquals(account, self.fake_account)

    @patch.object(MMAccount, "get_by_NIF")
    @patch.object(MMAccount, "create")
    @patch("api.account.AccountMapping")
    @patch("api.account.CustomerUser")
    @patch("api.account.UpdateTicketWithError")
    def test_create_mm_account_postal_code_ko(
        self,
        MockUpdateTicketWithArticle,
        MockCustomerUser,
        MockAccountMapping,
        mock_account_create,
        mock_account_get,
    ):
        """
        Check that if PostalCodeFormatException is raised when mapping, we send the
        MM error to OTRS.
        """
        pc = "AAAAA"

        MockCustomerUser.side_effect = self._otrs_customer_user_side_effect
        mock_account_get.side_effect = HTTPError(self.not_found_error)
        MockAccountMapping.side_effect = PostalCodeFormatException(pc)

        account = Account(
            self.session, self.ticket_data, self.customer_id, self.ticket_id
        )

        expected_error_msg = INVALID_PC_MSG.format(pc)
        expected_error_dct = {"missatge": expected_error_msg}

        self.assertRaisesRegex(
            PostalCodeFormatException,
            expected_error_msg,
            Account.get_or_create,
            account,
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id, expected_error_dct, "creació", "Account"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAccount, "get_by_NIF")
    @patch.object(MMAccount, "create")
    @patch("api.account.AccountMapping")
    @patch("api.account.CustomerUser")
    @patch("api.account.UpdateTicketWithError")
    def test_create_mm_account_province_ko(
        self,
        MockUpdateTicketWithArticle,
        MockCustomerUser,
        MockAccountMapping,
        mock_account_create,
        mock_account_get,
    ):
        """
        Check that if ProvinceNotFoundException is raised when mapping, we send the
        MM error to OTRS.
        """
        province_code = "ES-XI"

        MockCustomerUser.side_effect = self._otrs_customer_user_side_effect
        mock_account_get.side_effect = HTTPError(self.not_found_error)
        MockAccountMapping.side_effect = ProvinceNotFoundException(province_code)

        account = Account(
            self.session, self.ticket_data, self.customer_id, self.ticket_id
        )

        expected_error_msg = PROVINCE_NOT_FOUND_MSG.format(province_code)
        expected_error_dct = {"missatge": expected_error_msg}

        self.assertRaisesRegex(
            ProvinceNotFoundException,
            expected_error_msg,
            Account.get_or_create,
            account,
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id, expected_error_dct, "creació", "Account"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAccount, "get_by_NIF")
    @patch.object(MMAccount, "create")
    @patch("api.account.AccountMapping")
    @patch("api.account.CustomerUser")
    @patch("api.account.UpdateTicketWithError")
    def test_create_incomplete_mm_account_ko(
        self,
        MockUpdateTicketWithArticle,
        MockCustomerUser,
        MockAccountMapping,
        mock_account_create,
        mock_account_get,
    ):
        """
        Check that if AccountRequiredParamsError is raised by pymasmovil, we send
        this error to OTRS.
        """
        MockCustomerUser.side_effect = self._otrs_customer_user_side_effect
        mock_account_get.side_effect = HTTPError(self.not_found_error)
        MockAccountMapping.return_value = self.fake_account
        mock_account_create.side_effect = AccountRequiredParamsError("name")

        account = Account(
            self.session, self.ticket_data, self.customer_id, self.ticket_id
        )

        self.assertRaises(AccountRequiredParamsError, Account.get_or_create, account)

        MockAccountMapping.assert_called_once_with(
            self.ticket_data, self.customer_user_data
        )
        MockUpdateTicketWithArticle.assert_called_once()
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAccount, "get_by_NIF")
    @patch.object(MMAccount, "create")
    @patch("api.account.AccountMapping")
    @patch("api.account.CustomerUser")
    @patch("api.account.UpdateTicketWithError")
    def test_create_mm_account_ko(
        self,
        MockUpdateTicketWithArticle,
        MockCustomerUser,
        MockAccountMapping,
        mock_account_create,
        mock_account_get,
    ):
        """
        Check that if an account doesn't exist, but the MM request to create it fails,
        we send the MM error to OTRS and we reraised it.
        """
        MockCustomerUser.side_effect = self._otrs_customer_user_side_effect
        mock_account_get.side_effect = HTTPError(self.not_found_error)
        MockAccountMapping.return_value = self.fake_account
        mock_account_create.side_effect = HTTPError(self.sample_error)

        account = Account(
            self.session, self.ticket_data, self.customer_id, self.ticket_id
        )

        self.assertRaises(HTTPError, Account.get_or_create, account)

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id, self.expected_error, "creació", "Account"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()
