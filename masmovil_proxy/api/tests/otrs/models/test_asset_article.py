# coding: utf-8
import unittest

from otrs.models.asset_article import AssetArticle


class MMAssetArticleTestCase(unittest.TestCase):
    def test_article_instance(self):

        mm_asset = {
            "id": "111",
            "status": "en progreso",
            "productName": "Tarifa Ilimitada CM Voz Total Nov19",
        }

        article = AssetArticle(mm_asset)

        self.assertEqual(article.subject, "Línia de mòbil donada d'alta a MasMóvil")
        self.assertEqual(
            article.body,
            "id: 111\nproductName: Tarifa Ilimitada CM Voz Total Nov19"
            + "\nstatus: en progreso\n",
        )
