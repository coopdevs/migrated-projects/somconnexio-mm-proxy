# coding: utf-8
import unittest

from otrs.models.tariff_change_article import TariffChangeArticle


class TarifChangeArticleTestCase(unittest.TestCase):
    def test_article_instance(self):

        article = TariffChangeArticle("test body")

        self.assertEqual(article.subject, "Canvi de tarifa correctament realitzat")
        self.assertEqual(article.body, "test body")
