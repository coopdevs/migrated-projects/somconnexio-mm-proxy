# coding: utf-8
import unittest

from otrs.models.one_shot_bond_article import OneShotBondArticle


class OneShotBondArticleArticleTestCase(unittest.TestCase):
    def test_article_instance(self):

        article = OneShotBondArticle("test body")

        self.assertEqual(
            article.subject, "Abonament addicional one-shot correctament realitzat"
        )
        self.assertEqual(article.body, "test body")
