# coding: utf-8
import unittest

from otrs.models.order_item_article import OrderItemArticle


class OrderItemArticleTestCase(unittest.TestCase):
    def test_article_instance(self):

        article = OrderItemArticle("test body")

        self.assertEqual(article.subject, "Petició d'alta a MM entrada correctament")
        self.assertEqual(article.body, "test body")
