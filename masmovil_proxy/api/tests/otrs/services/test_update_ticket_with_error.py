# coding: utf-8
import unittest

from otrs.services.update_ticket_with_error import UpdateTicketWithError
from otrs.models.error_article import ErrorArticle


class UpdateTicketWithErrorTestCase(unittest.TestCase):
    def setUp(self):
        self.ticket_id = "1111"
        self.expected_error = {
            "statusCode": "400",
            "message": "El documentType no contiene un valor apto: 2",
            "fields": "documentType",
        }
        self.df_dct = {
            "dataIntroPlataforma": "",
            "dataActivacioLiniaMobil": "",
            "permetActivacio": "",
        }

    def test_run_create_order_item_error(self):

        df_dct = {
            "errorPeticio": 1,
            "enviarCreateOrderItem": 0,
        }
        self.df_dct.update(df_dct)

        error_update_service = UpdateTicketWithError(
            self.ticket_id, self.expected_error, "creació", "OrderItem"
        )

        self.assertEqual(error_update_service.df_dct, self.df_dct)
        self.assertEqual(error_update_service.ticket_id, self.ticket_id)
        self.assertIsInstance(error_update_service.article, ErrorArticle)

    def test_run_check_asset_cancelled(self):

        df_dct = {
            "errorActivacio": 1,
            "enviarCheckOrderItem": 0,
            "MMOrderItemId": "",
        }
        self.df_dct.update(df_dct)

        error_update_service = UpdateTicketWithError(
            self.ticket_id, self.expected_error, "resolució", "OrderItem"
        )

        self.assertEqual(error_update_service.df_dct, self.df_dct)
        self.assertEqual(error_update_service.ticket_id, self.ticket_id)
        self.assertIsInstance(error_update_service.article, ErrorArticle)

    def test_run_check_asset_error(self):

        df_dct = {
            "errorActivacio": 1,
            "enviarCheckOrderItem": 0,
        }
        self.df_dct.update(df_dct)

        error_update_service = UpdateTicketWithError(
            self.ticket_id, self.expected_error, "processement", "OrderItem"
        )

        self.assertEqual(error_update_service.df_dct, self.df_dct)
        self.assertEqual(error_update_service.ticket_id, self.ticket_id)
        self.assertIsInstance(error_update_service.article, ErrorArticle)

    def test_run_check_asset_not_found(self):

        df_dct = {
            "errorActivacio": 1,
            "enviarCheckOrderItem": 0,
            "MMOrderItemId": "",
        }
        self.df_dct.update(df_dct)

        error_update_service = UpdateTicketWithError(
            self.ticket_id, self.expected_error, "obtenció", "Asset"
        )

        self.assertEqual(error_update_service.df_dct, self.df_dct)
        self.assertEqual(error_update_service.ticket_id, self.ticket_id)
        self.assertIsInstance(error_update_service.article, ErrorArticle)

    def test_run_one_shot_bond_error(self):

        df_dct = {
            "dadesKO": 1,
        }

        error_update_service = UpdateTicketWithError(
            self.ticket_id,
            self.expected_error,
            "addició",
            "abonament one-shot addicional",
        )

        self.assertEqual(error_update_service.df_dct, df_dct)
        self.assertEqual(error_update_service.ticket_id, self.ticket_id)
        self.assertIsInstance(error_update_service.article, ErrorArticle)

    def test_run_change_tariff_error(self):

        df_dct = {
            "canviKO": 1,
        }

        error_update_service = UpdateTicketWithError(
            self.ticket_id, self.expected_error, "canvi de tarifa", "línia mòbil"
        )

        self.assertEqual(error_update_service.df_dct, df_dct)
        self.assertEqual(error_update_service.ticket_id, self.ticket_id)
        self.assertIsInstance(error_update_service.article, ErrorArticle)
