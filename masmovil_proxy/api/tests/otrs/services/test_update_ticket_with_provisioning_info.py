# coding: utf-8
import unittest
from mock import Mock

from otrs.services.update_ticket_with_provisioning_info import (
    UpdateTicketWithProvisioningInfo,
)
from otrs.models.asset_article import AssetArticle
from otrs.models.order_item_article import OrderItemArticle
from otrs.models.one_shot_bond_article import OneShotBondArticle
from otrs.models.tariff_change_article import TariffChangeArticle


class UpdateTicketWithProvisioningInfoTestCase(unittest.TestCase):
    def setUp(self):
        self.ticket_id = "1111"

    def test_run_created_order_item(self):

        order_item_id = "00001"
        phone_number = "68686868"
        order_item_df_dct = {
            "introPlataforma": 1,
            "enviarCreateOrderItem": 0,
            "MMOrderItemId": order_item_id,
            "liniaMobil": phone_number,
        }

        created_order_item_service = UpdateTicketWithProvisioningInfo(
            self.ticket_id,
            order_item_created_msg="test",
            mm_order_item_id=order_item_id,
            phone_number=phone_number
        )

        self.assertEqual(created_order_item_service.df_dct, order_item_df_dct)
        self.assertEqual(created_order_item_service.ticket_id, self.ticket_id)
        self.assertIsInstance(created_order_item_service.article, OrderItemArticle)

    def test_run_completed_order_item(self):

        billing_date = "2021-06-17 00:00:00"
        phone_num = "666666666"
        expected_asset = Mock(spec=["phone"])
        expected_asset.phone = phone_num
        asset_df_dct = {
            "contracteMobilActivat": 1,
            "enviarCheckOrderItem": 0,
            "liniaMobil": phone_num,
            "dataIniciFacturacio": billing_date,
        }

        new_phone_number_service = UpdateTicketWithProvisioningInfo(
            self.ticket_id, asset=expected_asset, billing_date=billing_date
        )

        self.assertEqual(new_phone_number_service.df_dct, asset_df_dct)
        self.assertEqual(new_phone_number_service.ticket_id, self.ticket_id)
        self.assertIsInstance(new_phone_number_service.article, AssetArticle)

    def test_run_one_shot_bond(self):

        one_shot_df_dct = {"dadesOK": 1, "RecuperarTarifaTRV": False}

        one_shot_bond_service = UpdateTicketWithProvisioningInfo(
            self.ticket_id, one_shot_bond_msg="test body"
        )

        self.assertEqual(one_shot_bond_service.df_dct, one_shot_df_dct)
        self.assertEqual(one_shot_bond_service.ticket_id, self.ticket_id)
        self.assertIsInstance(one_shot_bond_service.article, OneShotBondArticle)

    def test_run_one_shot_trv_bond(self):

        one_shot_df_dct = {
            "dadesOK": 1,
            "tarifaTRVOriginal": "0002",
            "RecuperarTarifaTRV": True
        }

        one_shot_bond_service = UpdateTicketWithProvisioningInfo(
            self.ticket_id, one_shot_bond_msg="test body", original_trv_tariff="0002"
        )

        self.assertEqual(one_shot_bond_service.df_dct, one_shot_df_dct)
        self.assertEqual(one_shot_bond_service.ticket_id, self.ticket_id)
        self.assertIsInstance(one_shot_bond_service.article, OneShotBondArticle)

    def test_run_tariff_changed(self):

        expected_message = "test message"
        expected_df_dct = {
            "canviOK": 1,
        }

        changed_tariff_service = UpdateTicketWithProvisioningInfo(
            self.ticket_id, tariff_change_msg=expected_message
        )

        self.assertEqual(changed_tariff_service.df_dct, expected_df_dct)
        self.assertEqual(changed_tariff_service.ticket_id, self.ticket_id)
        self.assertIsInstance(changed_tariff_service.article, TariffChangeArticle)
