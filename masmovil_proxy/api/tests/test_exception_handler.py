from mock import patch
from django.test import TestCase

from masmovil_proxy.exception_handler import failure_handler
from api.exceptions import (ExceptionToSendToOTRS,
                            PortabilityWithoutPhoneNumberException,
                            NewNumberRegistrationWithPhoneNumberException,
                            NullTariffChangeException,
                            PostalCodeFormatException)
from api.OTRS_catalan_messages import (PORTABILITY_WITHOUT_PHONE_NUMBER_MSG,
                                       PHONE_NUMBER_WITHOUT_PORTABILITY_MSG,
                                       NULL_CHANGE_TARIFF_REQUEST)


@patch("masmovil_proxy.exception_handler.UpdateTicketWithError")
class ExceptionHandler(TestCase):
    """
    TODO -> How can we test that asyncronously celery calls `failure_handler`
    with every exception raised?
    """

    def setUp(self):
        self.ticket_id = "1234"

    def test_call_OTRS_PortabilityWithoutPhoneNumberException(
            self, MockUpdateTicketWithArticle):
        exception = PortabilityWithoutPhoneNumberException(self.ticket_id)

        failure_handler("sender", "task-id", exception, {}, {}, "traceback", "einfo")

        self.assertIsInstance(exception, ExceptionToSendToOTRS)
        MockUpdateTicketWithArticle.assert_called_once_with(
            exception.ticket_id, {"missatge": PORTABILITY_WITHOUT_PHONE_NUMBER_MSG},
            "creació", "OrderItem"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    def test_call_OTRS_NullTariffChangeException(
            self, MockUpdateTicketWithArticle):
        exception = NullTariffChangeException(self.ticket_id)

        failure_handler("sender", "task-id", exception, {}, {}, "traceback", "einfo")

        self.assertIsInstance(exception, ExceptionToSendToOTRS)
        MockUpdateTicketWithArticle.assert_called_once_with(
            exception.ticket_id, {"missatge": NULL_CHANGE_TARIFF_REQUEST},
            "canvi de tarifa", "línia mòbil"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    def test_call_OTRS_NewNumberRegistrationWithPhoneNumberException(
            self, MockUpdateTicketWithArticle):
        phone_number = "688486434"
        exception = NewNumberRegistrationWithPhoneNumberException(self.ticket_id,
                                                                  phone_number)
        error_message = PHONE_NUMBER_WITHOUT_PORTABILITY_MSG.format(phone_number)

        failure_handler("sender", "task-id", exception, {}, {}, "traceback", "einfo")

        self.assertIsInstance(exception, ExceptionToSendToOTRS)
        MockUpdateTicketWithArticle.assert_called_once_with(
            exception.ticket_id, {"missatge": error_message}, "creació", "OrderItem"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    def test_call_OTRS_PostalCodeFormatException(
            self, MockUpdateTicketWithArticle):
        exception = PostalCodeFormatException("17852")

        failure_handler("sender", "task-id", exception, {}, {}, "traceback", "einfo")

        self.assertFalse(isinstance(exception, ExceptionToSendToOTRS))
        MockUpdateTicketWithArticle.assert_not_called()
