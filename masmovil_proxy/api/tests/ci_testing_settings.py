from __future__ import absolute_import, unicode_literals

import os

"""
Django settings for testing only tests in this project with gitlab-ci.
It avoids testing installed_apps own tests.
"""

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "fake-key"

# Application definition

INSTALLED_APPS = [
    "api.apps.ApiConfig",
    "django.contrib.staticfiles",
    "django.contrib.contenttypes",
    "django.contrib.messages",
    "django.contrib.admin",
    "django.contrib.auth",
]

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
]

ROOT_URLCONF = "masmovil_proxy.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.getenv("MM_PROXY_DB_NAME"),
        "USER": os.getenv("MM_PROXY_DB_USER"),
        "PASSWORD": os.getenv("MM_PROXY_DB_PASSWORD"),
        "HOST": os.getenv("MM_PROXY_DB_HOST"),
        "PORT": "",
    }
}
