from mock import patch

from django.test import TestCase
from django.urls import reverse


class AddOneShotBondsViewTestCase(TestCase):
    @patch("api.views.add_one_shot_bonds_view.add_one_shot_bonds")
    def test_patch(self, mock_add_one_shot_bonds_task):
        """
        Check that a patch request is done to the AddOneShotBondsView endpoint
        with the expected body.
        Also, prove that a task is enqueued with celery
        """
        self.new_request = {"test": "data"}

        url = reverse("add_one_shot_bonds")

        response = self.client.patch(
            url, data=self.new_request, content_type="application/json"
        )

        mock_add_one_shot_bonds_task.apply_async.assert_called_with(
            kwargs={"request_body": self.new_request}, queue="mm-proxy"
        )
        self.assertEqual(response.status_code, 200)
