from django.test import TestCase

from api.tariff import Tariff
from api.exceptions import TariffNotFoundException, InternationalTariffNotFoundException


class TariffTests(TestCase):
    fixtures = ["api/tests/fixtures/test_db"]

    def test_build_tariff_without_additional_bonds(self):

        tariff = Tariff("SE_SC_REC_MOBILE_T_0_800")

        self.assertEqual(tariff.mm_tariff, "T00000000000000002")
        self.assertEqual(tariff.additional_bonds, [])

    def test_build_tariff_with_additional_bonds(self):

        tariff = Tariff("SE_SC_REC_MOBILE_T_0_700")
        add_bond = {"id": "AB000000000000001", "qty": 1}

        self.assertEqual(tariff.mm_tariff, "T00000000000000001")
        self.assertEqual(tariff.additional_bonds, [add_bond])

    def test_build_tariff_with_promotions(self):

        tariff = Tariff("SE_SC_REC_MOBILE_T_0_900")
        promotion = {"id": "P000000000000001", "qty": 1}

        self.assertEqual(tariff.mm_tariff, "T00000000000000002")
        self.assertEqual(tariff.additional_bonds, [])
        self.assertEqual(tariff.promotions, [promotion])

    def test_build_tariff_with_international_calls(self):

        tariff = Tariff("SE_SC_REC_MOBILE_T_0_700", "8min")

        add_bond_list = [
            {"id": "AB000000000000001", "qty": 1},
            {"id": "IT0000000000000002", "qty": 1},
        ]

        self.assertEqual(tariff.additional_bonds, add_bond_list)

    def test_invalid_tariff_request(self):
        self.assertRaises(TariffNotFoundException, Tariff, "SE_SC_REC_MOBILE_T_FAKE")

    def test_invalid_international_tariff_request(self):
        self.assertRaises(
            InternationalTariffNotFoundException,
            Tariff,
            "SE_SC_REC_MOBILE_T_0_800",
            "35segons",
        )
