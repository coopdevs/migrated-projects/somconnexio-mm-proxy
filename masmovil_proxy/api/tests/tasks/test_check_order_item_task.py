from mock import patch, Mock
from django.test import TestCase
from pymasmovil.models.session import Session
from pymasmovil.errors.exceptions import (
    MissingLoginCredentialsError,
    AutenthicationError,
)
from api.tasks import check_order_item
from api.OTRS_catalan_messages import (
    EMPTY_ORDER_ITEM_ID_TO_CHECK_REQUEST,
    CANCELLED_ORDER_ITEM_MSG,
    ERROR_ORDER_ITEM_MSG
)


class CheckOrderItemTaskTestCase(TestCase):
    def setUp(self):
        self.mock_session = Mock(set_spec=[])
        self.phone = "666666666"

        self.request_body = {
            "ticket_id": 111,
            "mm_order_item_id": "123",
            "activate_date": "2021-06-17 00:00:00"
        }

        self.mock_order_item = Mock(set_spec=["id", "phone", "status"])
        self.mock_order_item.phone = self.phone
        self.mock_asset = Mock(set_spec=["id"])

    @patch(
        "api.tasks.UpdateTicketWithProvisioningInfo",
        return_value=Mock(set_spec=["run"]),
    )
    @patch("api.tasks.Asset", return_value=Mock(set_spec=["get_by_phone"]))
    @patch("api.tasks.OrderItem", return_value=Mock(set_spec=["get"]))
    @patch.object(Session, "create")
    def test_check_order_item_completed(
        self, mock_session_create, MockOrderItem, MockAsset, MockUpdateTicketWithArticle
    ):
        """
        Check that the order item status is completed, the corresponding asset is
        searched and sent to OTRS
        """
        mock_session_create.return_value = self.mock_session

        self.mock_order_item.status = "Completada"
        MockOrderItem.return_value.get.return_value = self.mock_order_item
        MockAsset.return_value.get_by_phone.return_value = self.mock_asset

        check_order_item(self.request_body)

        MockOrderItem.assert_called_once_with(
            self.mock_session, self.request_body.get("ticket_id")
        )
        MockOrderItem.return_value.get.assert_called_once_with(
            self.request_body.get("mm_order_item_id")
        )
        MockAsset.assert_called_once_with(
            self.mock_session, self.request_body.get("ticket_id")
        )
        MockAsset.return_value.get_by_phone.assert_called_once_with(self.phone)
        MockUpdateTicketWithArticle.assert_called_once_with(
            self.request_body.get("ticket_id"), asset=self.mock_asset,
            billing_date=self.request_body.get("activate_date")
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch("api.tasks.UpdateTicketWithError")
    @patch.object(Session, "create")
    def test_check_order_item_empty(
        self, mock_session_create, MockUpdateTicketWithArticle
    ):
        """
        Check that if no MMOrderItemID to check is recieved we warn OTRS
        """

        self.request_body.update({"mm_order_item_id": ""})

        check_order_item(self.request_body)

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.request_body.get("ticket_id"),
            {"missatge": EMPTY_ORDER_ITEM_ID_TO_CHECK_REQUEST},
            "resolució",
            "OrderItem",
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch("api.tasks.UpdateTicketWithError")
    @patch("api.tasks.OrderItem", return_value=Mock(set_spec=["get"]))
    @patch.object(Session, "create")
    def test_check_order_item_cancelled(
        self, mock_session_create, MockOrderItem, MockUpdateTicketWithArticle
    ):
        """
        Check that the order item status is cancelled, we warn OTRS
        """
        mock_session_create.return_value = self.mock_session

        self.mock_order_item.status = "Cancelada"

        MockOrderItem.return_value.get.return_value = self.mock_order_item

        check_order_item(self.request_body)

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.request_body.get("ticket_id"),
            {
                "missatge": CANCELLED_ORDER_ITEM_MSG.format(
                    self.request_body.get("mm_order_item_id"),
                )
            },
            "resolució",
            "OrderItem",
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch("api.tasks.UpdateTicketWithError")
    @patch("api.tasks.OrderItem", return_value=Mock(set_spec=["get"]))
    @patch.object(Session, "create")
    def test_check_order_item_rejected(
        self, mock_session_create, MockOrderItem, MockUpdateTicketWithArticle
    ):
        """
        Check that the order item status is error, we warn OTRS
        """

        mock_session_create.return_value = self.mock_session

        self.mock_order_item.status = "Error"

        MockOrderItem.return_value.get.return_value = self.mock_order_item

        check_order_item(self.request_body)

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.request_body.get("ticket_id"),
            {
                "missatge": ERROR_ORDER_ITEM_MSG.format(
                    self.request_body.get("mm_order_item_id")
                )
            },
            "processement",
            "OrderItem",
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch("api.tasks.UpdateTicketWithError")
    @patch.object(Session, "create")
    def test_autenthication_failure_MM(
        self, mock_session_create, MockUpdateTicketWithArticle
    ):
        """
        Check that an error is raised if we fail to login to MM API
        with our credentials.
        """
        mock_session_create.side_effect = AutenthicationError

        self.assertRaises(AutenthicationError, check_order_item, self.request_body)

        MockUpdateTicketWithArticle.assert_called_once()
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch("api.tasks.UpdateTicketWithError")
    @patch.object(Session, "create")
    def test_missing_MM_login_credentials(
        self, mock_session_create, MockUpdateTicketWithArticle
    ):
        """
        Check that an error is raised if we are missing some MM credentials to log in
        """
        mock_session_create.side_effect = MissingLoginCredentialsError("MM_USER")

        self.assertRaises(
            MissingLoginCredentialsError, check_order_item, self.request_body
        )

        MockUpdateTicketWithArticle.assert_called_once()
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()
