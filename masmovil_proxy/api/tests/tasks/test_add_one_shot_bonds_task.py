from mock import patch, Mock
from django.test import TestCase
from pymasmovil.models.session import Session

from api.tasks import add_one_shot_bonds


class AddOneShotBondsTaskTestCase(TestCase):
    @patch(
        "api.tasks.Asset",
        return_value=Mock(set_spec=["get_by_phone", "add_one_shot_additional_bond"]),
    )
    @patch.object(Session, "create")
    def test_add_one_shot_bonds(self, mock_session_create, MockAsset):
        """
        Check how we send to MM the petition to add an additional bond
        """

        request_body = {
            "ticket_id": "1234",
            "phone_number": "66868687",
            "product_code": "CH_SC_OSO_500MB_ADDICIONAL",
        }
        mock_asset = object()
        mock_session = Mock(set_spec=["id"])
        mock_session_create.return_value = mock_session
        MockAsset.return_value.get_by_phone.return_value = mock_asset

        add_one_shot_bonds(request_body)

        MockAsset.return_value.get_by_phone.assert_called_once_with(
            request_body["phone_number"],
        )
        MockAsset.return_value.add_one_shot_additional_bond.assert_called_once_with(
            mock_asset, request_body["product_code"]
        )
