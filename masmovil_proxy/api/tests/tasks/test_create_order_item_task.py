from mock import patch, Mock
from django.test import TestCase
from pymasmovil.models.session import Session
from pymasmovil.errors.exceptions import (
    MissingLoginCredentialsError,
    AutenthicationError,
)
from api.tasks import create_order_item
from api.OTRS_catalan_messages import ORDER_ITEM_SUCCESS_MSG


class CreateOrderItemTaskTestCase(TestCase):
    def setUp(self):
        self.mock_session = Mock(set_spec=[])
        self.name = "Guita"
        self.surname = "Guitona"
        self.account_id = "0001"
        self.order_item_id = "0002"
        self.ticket_id = 1111

        self.request_body = {
            "ticket_id": self.ticket_id,
            "customer_user": "11022",
            "ticket_account_data": {},
            "ticket_order_item_data": {
                "name": self.name,
                "surname": self.surname,
                "mobile_service_type": "portabilitat",
            },
        }

        self.expected_OTRS_mapped_fields = {
            "nomSoci": self.name,
            "cognom1": self.surname,
            "tipusServeiMobil": "portabilitat",
        }

        # Mock Account and assign values
        self.mock_account = Mock(set_spec=["id"])
        self.mock_account.id = self.account_id

        # Mock OrderItem and assign values
        self.mock_order_item = Mock(set_spec=["id", "phone_number"])
        self.mock_order_item.id = self.order_item_id
        self.mock_order_item.phone = "666777888"

    @patch("api.tasks.UpdateTicketWithProvisioningInfo")
    @patch("api.tasks.Account", return_value=Mock(set_spec=["get_or_create"]))
    @patch("api.tasks.time.sleep")
    @patch("api.tasks.OrderItem", return_value=Mock(set_spec=["create"]))
    @patch.object(Session, "create")
    def test_create_order_item(
        self,
        mock_session_create,
        MockOrderItem,
        mocksleep,
        MockAccount,
        MockUpdateTicketWithArticle,
    ):
        mock_session_create.return_value = self.mock_session

        MockAccount.return_value.get_or_create.return_value = self.mock_account
        MockOrderItem.return_value.create.return_value = self.mock_order_item
        create_order_item(self.request_body)

        MockAccount.assert_called_once_with(
            self.mock_session,
            self.request_body["ticket_account_data"],
            self.request_body["customer_user"],
            self.request_body["ticket_id"],
        )
        MockAccount.return_value.get_or_create.assert_called_once_with()
        mocksleep.assert_called_once_with(10)
        MockOrderItem.assert_called_once_with(
            self.mock_session, self.request_body["ticket_id"]
        )
        MockOrderItem.return_value.create.assert_called_once_with(
            self.mock_account,
            self.request_body["ticket_order_item_data"],
        )
        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id,
            order_item_created_msg=ORDER_ITEM_SUCCESS_MSG.format(
                self.order_item_id, self.account_id
            ),
            mm_order_item_id=self.order_item_id,
            phone_number=self.mock_order_item.phone
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch("api.tasks.UpdateTicketWithError")
    @patch.object(Session, "create")
    def test_autenthication_failure_MM(
        self, mock_session_create, MockUpdateTicketWithArticle
    ):
        mock_session_create.side_effect = AutenthicationError

        self.assertRaises(AutenthicationError, create_order_item, self.request_body)

        MockUpdateTicketWithArticle.assert_called_once()
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch("api.tasks.UpdateTicketWithError")
    @patch.object(Session, "create")
    def test_missing_MM_login_credentials(
        self, mock_session_create, MockUpdateTicketWithArticle
    ):
        mock_session_create.side_effect = MissingLoginCredentialsError("MM_USER")

        self.assertRaises(
            MissingLoginCredentialsError, create_order_item, self.request_body
        )

        MockUpdateTicketWithArticle.assert_called_once()
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()
