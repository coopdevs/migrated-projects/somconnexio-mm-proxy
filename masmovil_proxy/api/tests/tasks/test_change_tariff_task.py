from mock import patch, Mock
from django.test import TestCase
from pymasmovil.models.session import Session

from api.tasks import change_tariff


class ChangeTariffTaskTestCase(TestCase):
    @patch(
        "api.tasks.Asset", return_value=Mock(set_spec=["get_by_phone", "change_tariff"])
    )
    @patch.object(Session, "create")
    def test_change_tariff(self, mock_session_create, MockAsset):
        """
        Check how we send to MM the petition to change a tariff
        """

        request_body = {
            "ticket_id": "1234",
            "phone_number": "66868687",
            "product_code": "SE_SC_REC_MOBILE_T_0_555",
            "change_date": "",
        }
        mock_asset = Mock(spec=["id"])
        mock_asset.id = "000001"
        mock_session = Mock(set_spec=["id"])
        mock_session_create.return_value = mock_session
        MockAsset.return_value.get_by_phone.return_value = mock_asset

        change_tariff(request_body)

        MockAsset.return_value.get_by_phone.assert_called_once_with(
            request_body["phone_number"],
        )
        MockAsset.return_value.change_tariff.assert_called_once_with(
            mock_asset, request_body["product_code"], ""
        )
