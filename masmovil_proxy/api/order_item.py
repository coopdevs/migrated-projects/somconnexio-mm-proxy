from celery.utils.log import get_task_logger
from retry.api import retry_call
from bugsnag.celery import connect_failure_handler
from pymasmovil.models.order_item import OrderItem as MMOrderItem
from pymasmovil.errors.http_error import HTTPError
from pymasmovil.errors.exceptions import (NewLineRequestRequiredParamsError,
                                          OrderItemNotFoundByICC)

from api.new_line_request_mapping import NewLineRequestMapping
from api.OTRS_catalan_messages import INCOMPLETE_ORDER_ITEM_MSG
from api.exceptions import (
    TariffNotFoundException,
    InternationalTariffNotFoundException,
)
from otrs.services.update_ticket_with_error import UpdateTicketWithError

logger = get_task_logger(__name__)
connect_failure_handler()


class OrderItem:
    def __init__(self, session, ticket_id):
        self.session = session
        self.ticket_id = ticket_id

    def get(self, order_item_id):
        """
        Get an OrderItem from MM with an order_item id
        """
        order_item = MMOrderItem.get(self.session, order_item_id)

        logger.info(
            "OrderItem correctly recieved with id: {} and status: {}".format(
                order_item.id, order_item.status
            )
        )

        return order_item

    def create(self, account, ticket_data):
        """
        Try to create an OrderItem to MM with our mapped request data
        """

        # Create a NewLineRequestMapping object with the parameters used in
        # `MMOrderItem.create()`
        try:
            self.mapped_new_line_request = NewLineRequestMapping(
                ticket_data, account, self.ticket_id)
        except (
            TariffNotFoundException,
            InternationalTariffNotFoundException,
        ) as exception:
            UpdateTicketWithError(
                self.ticket_id, {"missatge": exception.message}, "creació", "OrderItem"
            ).run()
            raise exception

        try:
            MMOrderItem.create(
                self.session, account, **self.mapped_new_line_request.__dict__
            )
        except NewLineRequestRequiredParamsError as exception:
            UpdateTicketWithError(
                self.ticket_id,
                {"missatge": INCOMPLETE_ORDER_ITEM_MSG.format(exception.message)},
                "creació",
                "OrderItem",
            ).run()
            raise exception
        except HTTPError as error:
            UpdateTicketWithError(
                self.ticket_id, error.__dict__, "creació", "OrderItem"
            ).run()
            raise error

        try:
            ICC = self.mapped_new_line_request.lineInfo[0]["iccid"]
            # Get the recently created order-item and
            # retry 3 times with a 20 seconds delay in between attemps,
            # to avoid not being able to get the just created order-item
            # from the MM platform because of delay issues

            new_order_item = retry_call(
                account.get_order_item_by_ICC,
                fargs=[self.session, ICC, True],
                exceptions=(HTTPError, OrderItemNotFoundByICC),
                tries=3,
                delay=20,
            )
        except HTTPError as error:
            UpdateTicketWithError(
                self.ticket_id, error.__dict__, "obtenció", "OrderItem"
            ).run()
            raise error
        except OrderItemNotFoundByICC as exception:
            UpdateTicketWithError(
                self.ticket_id, {"missatge": exception.message}, "obtenció", "OrderItem"
            ).run()
            raise exception

        logger.info(
            "OrderItem created with id: {} from transaction id: {}".format(
                new_order_item.id, self.mapped_new_line_request.transactionId
            )
        )

        return new_order_item
